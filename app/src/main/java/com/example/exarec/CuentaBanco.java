package com.example.exarec;

public class CuentaBanco {
    private String numeroCuenta;
    private String nombreCliente;
    private String banco;
    private double saldo;

    // Constructor
    public CuentaBanco(String numeroCuenta, String nombreCliente, String banco, double saldo) {
        this.numeroCuenta = numeroCuenta;
        this.nombreCliente = nombreCliente;
        this.banco = banco;
        this.saldo = saldo;
    }

    // Métodos
    public double consultarSaldo() {
        return saldo;
    }

    public void depositar(double cantidad) {
        if (cantidad > 0) {
            saldo += cantidad;
        }
    }

    public boolean retirar(double cantidad) {
        if (cantidad > 0 && cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        }
        return false;
    }

    // Getters y Setters
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public String getBanco() {
        return banco;
    }

    public double getSaldo() {
        return saldo;
    }
}
